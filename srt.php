<?
$handle = fopen("correct_srt.php", "w");
if(!isset($argv[1]) ){
exit("PLease Enter File Name\n\n");
}
$filename = $argv[1];
if(!isset($argv[2])){
exit("Please Enter the Delay, (for ex +30 if the srt is ahead 30 sec or -30 id it is delaying )\n\n");
}

$filename = $argv[1];
$delay = $argv[2];
$lines = file($filename);

foreach ($lines as $line_num => $line) {
	if(strpos($line,"-->") !== false){
		$line = modifyTimestamp($line, $delay). PHP_EOL;
	}
	fwrite($handle, $line);
}
fclose($handle);
function modifyTimestamp($line, $sec_delay){
	$timestamp = explode(" --> ", $line);
	return  date('H:i:s',strtotime(substr(trim($timestamp[0]), 0,-4))+ $sec_delay ). substr(trim($timestamp[0]), -4)  . ' --> ' .  date('H:i:s', strtotime(substr(trim($timestamp[1]), 0,-4))+ $sec_delay) . substr(trim($timestamp[1]), -4);
}
?>
